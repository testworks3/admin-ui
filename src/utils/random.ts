const randomUniqueString = new Set()

export const getRandomString = (unique = false): string => {
  const random = (Math.random() + 1).toString(36).substring(7);

  if (unique && randomUniqueString.has(random)) {
    return getRandomString(unique)
  } else if (unique) {
    randomUniqueString.add(random)
  }

  return random
}