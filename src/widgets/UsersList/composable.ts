import {
  useAUIModal,
  useAUIForm,
} from '@/components/ui'
import { computed, ref, watch } from 'vue'
import { useUsersStore } from '@/stores/users.ts'
import { createTable } from '@/widgets/UsersList/table.tsx'

export const useUsersList = () => {
  const usersStore = useUsersStore()

  const { Dialog: AddUserDialog, open: openAddUserDialog, close: closeAddUserDialog } = useAUIModal()
  const { Form: AddUserForm, fields: userFormFields, submit: userFormSubmit, isValid: isUserFormValid, reset: userFormReset  } = useAUIForm<{
    name: string,
    email: string,
    password: string,
    verifyPassword: string,
  }>({
    name: {
      label: 'Name',
      type: 'text',
    },
    email: {
      label: 'Email',
      type: 'text',
    },
    password: {
      label: 'Password',
      type: 'password',
    },
    verifyPassword: {
      label: 'Verify password',
      type: 'password',
    },
  })

  ;[userFormFields.name, userFormFields.email, userFormFields.password, userFormFields.verifyPassword].forEach((field) => {
    field.addValidation('required', (input) => {
      return !input ? 'Field is required' : true
    })
  })

  userFormFields.name.addValidation('minLength', (input) => {
    return input.length < 3 ? 'Name should contain at least 3 symbols' : true
  })

  userFormFields.email.addValidation('isEmail', (input) => {
    return !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(input) ? 'Email is invalid' : true
  })

  userFormFields.password.addValidation('isPassword', (input) => {
    return input.length < 8 ? 'Field should contain at least 8 symbols' : true
  })

  userFormFields.verifyPassword.addValidation('isSamePassword', (input) => {
    const result = input !== userFormFields.password.value.value ? 'Passwords are not matching' : true

    if (typeof result !== 'string' && userFormFields.password.valid.value === 'Passwords are not matching') {
      userFormFields.password.valid.value = true
    } else if (typeof userFormFields.password.valid.value !== 'string') {
      userFormFields.password.valid.value = result
    }

    return result
  })

  userFormFields.password.addValidation('isSamePassword', (input) => {
    const result = input !== userFormFields.verifyPassword.value.value ? 'Passwords are not matching' : true

    if (typeof result !== 'string' && userFormFields.verifyPassword.valid.value === 'Passwords are not matching') {
      userFormFields.verifyPassword.valid.value = true
    } else if (typeof userFormFields.verifyPassword.valid.value !== 'string') {
      userFormFields.verifyPassword.valid.value = result
    }

    return result
  })

  const isEdit = ref(false)

  watch(isEdit, () => {
    userFormFields.email.isDisabled.value = isEdit.value
  })

  const handleUserFormSubmit = async () => {
    const result = await userFormSubmit()

    if (!result) {
      return
    }

    if (isEdit.value) {
      usersStore.editUser({
        name: result.name,
        email: result.email,
        password: result.password,
      })
    } else {
      usersStore.addUser({
        name: result.name,
        email: result.email,
        password: result.password,
      })
    }

    closeAddUserDialog()
  }

  const { tableHeaders, tableData } = createTable(usersStore, (user) => {
    userFormFields.name.value.value = user.name
    userFormFields.email.value.value = user.email
    userFormFields.password.value.value = user.password
    userFormFields.verifyPassword.value.value = user.password

    isEdit.value = true
    openAddUserDialog()
  })

  const modalName = computed(() => isEdit.value ? 'Edit User' : 'Add user')

  return {
    AddUserDialog,
    openAddUserDialog,
    closeAddUserDialog,
    AddUserForm,
    userFormFields,
    userFormReset,
    handleUserFormSubmit,
    isUserFormValid,
    tableHeaders,
    tableData,
    modalName,
    isEdit
  }
}