import './styles.scss'
import { defineComponent, nextTick } from 'vue'

import {
  AUIButtonComponent,
  AUITableComponent,
} from '@/components/ui'
import { useUsersList } from './composable'
export default defineComponent({
  name: 'TestworkUsersList',

  setup() {
    const { AddUserDialog, AddUserForm, openAddUserDialog, handleUserFormSubmit, tableData, tableHeaders, modalName, isUserFormValid, userFormReset, userFormFields, isEdit } = useUsersList()

    const openDialog = () => {
      isEdit.value = false;
      userFormReset()
      openAddUserDialog()
    }

    const onFormOpened = async () => {
      await nextTick()

      if (userFormFields.name.el.value) {
        userFormFields.name.el.value.focus()
      }
    }

    return () => (
      <div class="users-list">
        <AUIButtonComponent onClick={openDialog}>Add User</AUIButtonComponent>
        <AUITableComponent tableHeaders={tableHeaders} tableData={tableData.value} />
        <AddUserDialog onOpened={onFormOpened}>{{
          title: () => modalName.value,
          default: () => <AddUserForm />,
          submits: () => <AUIButtonComponent onClick={handleUserFormSubmit} disabled={!isUserFormValid.value}>Submit</AUIButtonComponent>
        }}</AddUserDialog>
      </div>
    )
  }
})