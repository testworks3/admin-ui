import { type User, useUsersStore } from '@/stores/users.ts'
import {
  AUIButtonComponent,
  AUIIconEdit,
  AUIIconDelete,
  type AUITablePropTableData,
  type AUITablePropTableHeaders,
} from '@/components/ui'
import { computed } from 'vue'

export const createTable = (usersStore: ReturnType<typeof useUsersStore>, onEdit: (user: User) => void) => {
  const tableHeaders: AUITablePropTableHeaders = [{
    name: 'Name',
  }, {
    name: 'Email'
  }, {
    name: 'Action',
    size: 7.5,
  }]
  
  const onDelete = (user: User) => {
    const isConfirmed = window.confirm('Are you sure?')
    
    if (isConfirmed) {
      usersStore.deleteUser(user);
    }
  }

  const tableData = computed<AUITablePropTableData>(() => {
    const data: AUITablePropTableData = []
    usersStore.users.forEach((user) => {
      data.push(
        {
          component: () => user.name
        },
        {
          component: () => user.email
        }, {
          component: () => (<div class="table-controls">
            <AUIButtonComponent color="yellow" size="no-p" onClick={() => onEdit(user)}><AUIIconEdit/></AUIButtonComponent>
            <AUIButtonComponent color="red" size="no-p" onClick={() => onDelete(user)}><AUIIconDelete/></AUIButtonComponent>
          </div>)
        }
      )
    })

    return data
  })

  return {
    tableHeaders,
    tableData
  }
}