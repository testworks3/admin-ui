import { computed, defineComponent, type InjectionKey, type Ref, type ComputedRef, ref, inject, provide } from 'vue'
import type { AUIFormFieldTypes } from './fieldBuilders'

import { builders as fields } from './fieldBuilders'
import { getRandomString } from '@/utils'
import type { AUIFormInjectionKey } from '@/components/ui'
import { createValidator } from '@/components/ui/AUIForm/validator'

export type AUIFormFieldParam = {
  label: string,
  type: AUIFormFieldTypes,
}

export type AUIFormField = {
  id: string
  value: Ref<unknown>
  label: Ref<string>
  valid: Ref<string | boolean>
  isFocus: Ref<boolean>
  isDisabled: Ref<boolean>
  isError: ComputedRef<boolean>
  validate: () => Promise<string | boolean>
  isErrorVisible: ComputedRef<boolean>
  errorText: ComputedRef<string>
  formSymbol?: AUIFormInjectionKey
  fieldSymbol?: AUIFormFieldInjectionKey
  el?: Ref<HTMLElement | undefined>
}
export type AUIFormFieldInjectionKey = InjectionKey<AUIFormField>
export const createField = (fieldParam: AUIFormFieldParam, formInjectionKey: AUIFormInjectionKey) => {
  const randomId = getRandomString(true)
  const field: AUIFormFieldInjectionKey = Symbol(randomId)

  const el = ref<HTMLElement>()

  const value = ref<unknown>('')
  const label = ref(fieldParam.label)
  const valid = ref<string | boolean>(true)

  const { validateAll, addValidation } = createValidator()
  const validate = async () => {
    const result = await validateAll(value.value)
    valid.value = result
    return result
  }

  const errorText = computed(() => typeof valid.value === 'string' ? valid.value : '')
  const isError = computed(() => !!errorText.value || !valid.value)

  const isFocus = ref(false)
  const isDisabled = ref(false)

  return {
    value,
    label,
    valid,
    id: randomId as string,
    errorText,
    isError,
    isFocus,
    isDisabled,
    validate,
    addValidation,
    el,
    component: () => defineComponent({
      name: 'AUIFormField',

      setup() {
        const form = inject(formInjectionKey)

        if (!form) {
          throw new Error('AUIForm Bug: form is not injectable')
        }

        const isErrorVisible = computed(() => form.isValidating.value && isError.value)

        const fieldData: AUIFormField = {
          id: randomId,
          value,
          label,
          valid,
          isFocus,
          isError,
          isDisabled,
          isErrorVisible,
          validate,
          errorText,
          formSymbol: formInjectionKey,
          fieldSymbol: field,
          el,
        }
        provide(field, fieldData)

        return fields[fieldParam.type].build(fieldData)
      }
    })
  }
}