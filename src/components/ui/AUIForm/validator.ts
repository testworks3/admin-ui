
export type AUIFormValidation = (input: any) => Promise<boolean | string> | boolean | string
export const createValidator = () => {
  const validations = new Map<string, AUIFormValidation>();

  const addValidation = (key: string, func: AUIFormValidation) => {
    validations.set(key, func);
  };

  const validateAll = async (value: any) => {
    const count = validations.size
    let result: string | boolean = count === 0

    if (!count) {
      return result
    }

    for (const set of validations.entries()) {
      result = await set[1](value)

      if (typeof result === 'string' || !result) {
        return result
      }
    }

    return result
  };

  return {
    validations,
    addValidation,
    validateAll
  };
};