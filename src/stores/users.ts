import { ref } from 'vue'
import { defineStore } from 'pinia'

export type User = {
  name: string
  email: string
  password: string
}
export const useUsersStore = defineStore('users', () => {
  const users = ref<Array<User>>([{
    name: 'Petr Pavel',
    email: 'petr@pavel.nato',
    password: '11111111',
  }])
  function addUser(userParam: User) {
    if (users.value.find((user) => user.email === userParam.email)) {
      throw new Error('User already exist')
    }
    users.value.push(userParam)
  }

  function editUser(userParam: User) {
    const userIndex = users.value.findIndex((user) => user.email === userParam.email)

    if (userIndex === -1) {
      throw new Error('User not found')
    }

    users.value[userIndex] = {
      ...userParam
    }
  }

  function deleteUser(userParam: User) {
    const userIndex = users.value.findIndex((user) => user.email === userParam.email)

    if (userIndex === -1) {
      throw new Error('User not found')
    }

    users.value = users.value.filter((_item, i) => i !== userIndex)

    console.log(users.value.filter((_item, i) => i !== userIndex))
  }

  return { users, addUser, editUser, deleteUser }
})
