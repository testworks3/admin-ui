import './styles.scss'
import { defineComponent, computed, type PropType } from 'vue'

export type AUITablePropTableHeaders = Array<{
  name: string
  component?: never
  size?: number
} | {
  name?: never
  component: () => string | ReturnType<typeof defineComponent>
  size?: number
}>

export type AUITablePropTableData = Array<{
  onClick?: () => void,
  component?: () => string | ReturnType<typeof defineComponent>
}>
export const AUITableComponent = defineComponent({
  name: 'AUITable',

  props: {
    tableHeaders: {
      type: Array as PropType<AUITablePropTableHeaders>,
      default: () => ([])
    },

    tableData: {
      type: Array as PropType<AUITablePropTableData>,
      default: () => ([])
    }
  },

  setup(props) {
    const gridStyles = computed(() => {
      const headerColumns: Array<string> = []
      const templateColums = props.tableHeaders.reduce((str, item, i) => {
        str += item.size && `${item.size}em` || 'auto'
        str += ' '
        headerColumns.push(`header-${i}`)
        return str
      }, '')

      let rowsTemplateArea = ''
      for (let i = 0; i < (props.tableData.length) / props.tableHeaders.length; i ++) {
        const localStrArray = props.tableHeaders.map(() => `row-${i}`)
        rowsTemplateArea += `"${localStrArray.join(' ')}"\n`
      }

      if (!rowsTemplateArea) {
        const localStrArray = props.tableHeaders.map(() => `empty-row`)
        rowsTemplateArea += `"${localStrArray.join(' ')}"\n`
      }

      const gridTemplateAreas = `
        "${headerColumns.join(' ')}"
        ${rowsTemplateArea}
      `

      return {
        display: 'grid',
        'grid-template-columns': templateColums,
        'grid-template-rows': 'auto',
        'grid-template-areas': gridTemplateAreas,
      }
    })

    return () => <>
      <div class="aui-table" style={gridStyles.value}>
        {props.tableHeaders.map((header) => (
          <div class="table-header table-item">{
            typeof header.name === 'string' ? header.name : header.component()
          }</div>
        ))}
        {props.tableData.map((data) => (
          <div class="table-item" onClick={data.onClick}>{
            data.component ? data.component() : ''
          }</div>
        ))}
        {props.tableData.length === 0
          ? <div class="table-item" style={{
            gridArea: 'empty-row',
            justifyContent: 'center'
          }}>Table is empty</div>
          : null}
      </div>
    </>
  }
})