import './styles.scss'
import { defineComponent, type PropType, inject } from 'vue'
import type { AUIFormFieldInjectionKey } from '@/components/ui/AUIForm/field'

type PossibleInputTypes = 'text' | 'password'
export const build = (injectionKey: AUIFormFieldInjectionKey) => defineComponent({
  name: 'AUIBaseFieldInput',

  props: {
    id: {
      type: String,
    },
    value: {
      type: String,
    },
    type: {
      type: String as PropType<PossibleInputTypes>
    },
  },

  emits: ['focus', 'blur', 'input'],

  setup(props, ctx) {
    const fieldData = inject(injectionKey)

    if (!fieldData) {
      throw new Error('AUIForm base input: unreachable fieldData')
    }

    const onInput = (value: string) => {
      ctx.emit('input', value)
    }

    return () => (
      <div class={`aui-input`}>
        <div class="aui-input_holder">
          <input disabled={fieldData.isDisabled.value} ref={fieldData.el} id={props.id} type={props.type} value={props.value} onInput={(e) => {
            // @ts-expect-error
            onInput(e.target.value)
          }} onBlur={() => ctx.emit('blur')} onFocus={() => ctx.emit('focus')} />
          {fieldData.errorText.value && fieldData.isErrorVisible.value ? <div class="error-text">{fieldData.errorText.value}</div> : ''}
        </div>
      </div>
    )
  }
})