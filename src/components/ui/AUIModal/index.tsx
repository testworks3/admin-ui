import './styles.scss'
import { computed, defineComponent, ref, type Ref, onUnmounted, onMounted, watch, Teleport } from 'vue'

import { AUIButtonComponent } from '../index'

export type AUIModalDialogElement = ReturnType<typeof ref<HTMLDivElement>>
const getDialog = (dEl: AUIModalDialogElement) => {
  const dialog = dEl.value
  if(!dialog) {
    throw new Error('AUIModal is not mounted')
  }

  return dialog
}

const createDialog = (dEl: AUIModalDialogElement, params: AUIModalParams) => {
  const isOpen = ref(params.isOpen || false)
  function calculatePosition (refs: Record<'top' | 'left', Ref<number>>) {
    if (!dEl.value) {
      return
    }

    const { top, left } = refs

    const { clientWidth, clientHeight } = document.documentElement
    const { width, height } = dEl.value.getBoundingClientRect()

    /** TODO: Pattern "Strategy" with "Builder".
     * WHY: Currently we are positioning only on document center.
     * WHAT: We can create Factory of stategyBuilders to accept parameter from useAUIModal<'center' | 'top' | 'top:left' | 'top:right' ...>
     * Not the purpose of this testwork (will be done later or maybe never)
    */
    top.value = clientHeight / 2 - height / 2
    left.value = clientWidth / 2 - width / 2
  }

  const open = () => {
    isOpen.value = true
  }

  const close = () => {
    isOpen.value = false
  }

  return {
    isOpen,
    open,
    close,
    Dialog: defineComponent({
      name: 'AUIModal',

      props: {
        width: {
          type: String,
          default: 'var(--aui-default-modal-width)'
        },
        height: {
          type: String,
          default: 'var(--aui-default-modal-height)'
        }
      },

      emits: ['closed', 'opened'],

      setup(props, ctx) {
        const top = ref(0)
        const left = ref(0)

        function createResize() {
          calculatePosition({
            top,
            left
          })
        }

        onMounted(() => {
          createResize()
        })

        const dismiss = watch(isOpen, () => {
          queueMicrotask(() => {
            createResize()
          })
        })

        window.addEventListener('resize', createResize)
        onUnmounted(() => {
          window.removeEventListener('resize', createResize)
          dismiss()
        })

        watch(isOpen, () => {
          if (isOpen.value) {
            ctx.emit('opened')
          } else {
            ctx.emit('closed')
          }
        })

        const proceedClick = (event: MouseEvent) => {
          const dialog = dEl.value
          if (!dialog) {
            return
          }
          const { clientX, clientY } = event
          const { x, y, width, height } = dialog.getBoundingClientRect()

          const minX = x;
          const maxX = x + width;
          const minY = y;
          const maxY = y + height;
          const isClickInside = clientX >= minX && clientX <= maxX && clientY >= minY && clientY <= maxY
          if (!isClickInside) {
            close()
          }
        }

        return () => (
          isOpen.value ?
            <Teleport to="body">
              <div ref={dEl} class={`aui-modal`} style={{
                minWidth: `${props.width}`,
                minHeight: `${props.height}`,
                top: `${top.value}px`,
                left: `${left.value}px`
              }} onClick={proceedClick}>
                <div class="aui-modal_content">
                  <div class="aui-modal_content-title">
                    <div class="aui-modal_content-title_content">
                      {ctx.slots.title ? ctx.slots.title() : null}
                    </div>
                    <div class="aui-modal_content-title_controls">
                      <AUIButtonComponent color="red" size="small" onClick={() => close()}><span
                        class="aui-modal_content-title_controls__font">X</span></AUIButtonComponent>
                    </div>
                  </div>
                  <div class="aui-modal_content-content">
                    {ctx.slots.default ? ctx.slots.default() : 'Empty'}
                  </div>
                  {ctx.slots.submits ?
                    <div class="aui-modal_content-submits">
                      { ctx.slots.submits() }
                    </div>
                    : null
                  }
                </div>
              </div>
            </Teleport>
            : null
        )
      },
    })
  }
}

export type AUIModalParams = Partial<{
  isOpen: boolean
}>
export const useAUIModal = (params: AUIModalParams = {}) => {
  const dEl = ref<HTMLDivElement>()

  const { Dialog, open, close, isOpen} = createDialog(dEl, params)
  return {
    Dialog,
    open,
    close,
    el: computed(() => getDialog(dEl)),
    isOpen,
  }
}