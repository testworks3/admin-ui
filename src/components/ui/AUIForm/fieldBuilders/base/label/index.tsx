import './styles.scss'
import { defineComponent, inject } from 'vue'
import type { AUIFormFieldInjectionKey } from '@/components/ui/AUIForm/field'
export const build = (injectionKey: AUIFormFieldInjectionKey) => defineComponent({
  name: 'AUIBaseFieldLabel',

  props: {
    for: {
      type: String,
    },
    text: {
      type: String,
    }
  },

  setup(props) {
    const field = inject(injectionKey)
    console.log(field)
    return () => (
      <div class="aui-label">
        <div class="aui-label_holder">
          <label for={props.for}>{ props.text }</label>
        </div>
      </div>
    )
  }
})