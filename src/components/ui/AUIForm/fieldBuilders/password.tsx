import { createBaseField, type FieldBuilder } from './index'
import { label } from './base'

export const build: FieldBuilder = (fieldData) => {
  const { fieldSymbol } = fieldData

  if (!fieldSymbol) {
    throw new Error('Missing dependency fieldSymbol')
  }

  const Field = createBaseField('password', fieldSymbol)
  const Label = label.build(fieldSymbol)

  if (!Field) {
    throw new Error('Unsupported base field')
  }

  const field = fieldData

  if (!field) {
    throw new Error('Unreachable field attempted to build')
  }

  const valueType = typeof field.value.value
  if (!(valueType === 'string' || valueType === 'number')) {
    throw new Error('Value type is incompatible to this field')
  }

  const value = () => field.value.value as string
  const onBlur = () => {
    field.isFocus.value = false
  }
  const onFocus = () => {
    field.isFocus.value = true
  }

  const onInput = (value: string) => {
    field.value.value = value
    field.validate()
  }

  return () => (
    <div class={`aui-form_field ${field.isErrorVisible.value ? 'error' : ''} ${field.isFocus.value ? 'focus' : ''}`}>
      <Label for={field.id} text={field.label.value} />
      <Field id={field.id} value={value()} type='password' onBlur={onBlur} onFocus={onFocus} onInput={onInput}/>
    </div>
  )
}