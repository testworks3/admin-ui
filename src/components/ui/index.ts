import AUIButtonComponent from './AUIButton.vue'
export * from './AUITable'
export * from './AUIModal'
export * from './AUIForm'

import AUIIconEdit from './AUIIcons/AUIIconEdit.vue'
import AUIIconDelete from './AUIIcons/AUIIconDelete.vue'

export {
  AUIButtonComponent,
  AUIIconEdit,
  AUIIconDelete,
}