import './styles.scss'
import { defineComponent, h, ref, provide, type InjectionKey, type Ref, computed } from 'vue'
import type { AUIFormFieldParam } from './field'
import { createField } from './field'

export type AUIFormFieldsParam<T extends Record<string, any>> = Record<keyof T, AUIFormFieldParam>
export type AUIFormInjectionKey = InjectionKey<{
  isValidating: Ref<boolean>
}>
function createForm <T extends Record<string, any>>(fieldsParam: AUIFormFieldsParam<T>) {
  const form: AUIFormInjectionKey = Symbol('form')
  const isValidating = ref(false)

  provide(form, {
    isValidating,
  })

  // @ts-ignore
  const fieldsDefault: Record<keyof AUIFormFieldsParam<T>, any> = {}
  const fieldsUndefined: Partial<Record<keyof AUIFormFieldsParam<T>, ReturnType<typeof createField>>> = {}
  for (const [fieldKey, field] of Object.entries(fieldsParam)) {
    const fieldLocal = createField(field, form)

    if (!fieldsUndefined[fieldKey]) {
      // @ts-expect-error
      fieldsUndefined[fieldKey] = fieldLocal
      // @ts-expect-error
      fieldsDefault[fieldKey] = fieldLocal.value.value
    } else {
      throw new Error(`Can't create AUIForm: Key ${fieldKey} is not unique`)
    }
  }
  const fields: Record<keyof AUIFormFieldsParam<T>, ReturnType<typeof createField>> = fieldsUndefined as Record<keyof AUIFormFieldsParam<T>, ReturnType<typeof createField>>

  const isValid = computed(() => {
    if (!isValidating.value) {
      return true
    }
    for (const field of Object.values(fields)) {
      const isError = typeof field.valid.value === 'string' || !field.valid.value
      if (isError) {
        return false
      }
    }
    return true
  })
  async function submit() {
    isValidating.value = true
    await Promise.all(Object.values(fields).map(field => field.validate()))

    if (!isValid.value) {
      return
    }

    const formValues: Partial<T> = {}
    for (const key in fields) {
       formValues[key] = fields[key].value.value as T[typeof key]
    }

    return formValues as T;
  }

  const reset = () => {
    for (const key in fields) {
      fields[key].value.value = fieldsDefault[key]
    }
    isValidating.value = false
  }

  return {
    fields,
    isValidating,
    isValid,
    submit,
    reset,
    Form: defineComponent({
      name: 'AUIForm',

      setup() {
        return () => (
          <div class="aui-form">
            {
              Object.keys(fieldsParam).map((fieldKey) => {
                return h(fields[fieldKey] ? fields[fieldKey].component() : '')
              })
            }
          </div>
        )
      }
    })
  }
}
export function useAUIForm <T extends Record<string, any>>(fieldsParam: Record<keyof T, AUIFormFieldParam>) {
  if (!Object.keys(fieldsParam).length) {
    throw new Error('Can\'t create AUIForm: AUIForm should have at least one field')
  }

  const { Form, fields, isValidating, isValid, submit, reset, } = createForm<T>(fieldsParam)
  return {
    Form,
    fields,
    isValidating,
    isValid,
    submit,
    reset
  }
}