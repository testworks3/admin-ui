import { createRouter, createWebHistory } from 'vue-router';
import Home from './view/RouteHome.vue'
import Users from './view/RouteUsers.vue'

// Define route components
const routes = [
  {
    path: '/', component: Home, children: [
      {
        path: 'users', component: Users, meta: {
          breadName: 'Users',
        },
      },
    ], meta: {
      breadName: 'Home',
    },
  },
];

// Create the router instance
const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;