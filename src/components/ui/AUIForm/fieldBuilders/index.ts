import * as baseBuilders from './base'
import * as password from './password'
import * as text from './text'
import type { AUIFormField, AUIFormFieldInjectionKey } from '@/components/ui/AUIForm/field'

export type AUIFormFieldTypes = 'text' | 'password'
export const createBaseField = (type: AUIFormFieldTypes, injectionKey: AUIFormFieldInjectionKey) => {
  const typesMap: Record<AUIFormFieldTypes, keyof typeof baseBuilders> = {
    text: 'input',
    password: 'input'
  }

  const builder = baseBuilders[typesMap[type]]
  if (builder && (type === 'text' || type === 'password')) {
    return baseBuilders.input.build(injectionKey)
  }
}

export type FieldBuilder = (fieldData: AUIFormField) => unknown

export const builders: Record<AUIFormFieldTypes, { build: FieldBuilder }> = {
  password,
  text
}